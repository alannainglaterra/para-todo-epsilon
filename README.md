## para todo epsilon

Repositório do site www.paratodoepsilon.com.br

## Contribuições

Submeta um merge request para publicar algum novo artigo. Por enquanto o site está escrito em plain-html. Futuramente adicionarei um motor de blog estático.

## Contato

alandemaria (at) gmail (dot) com ou abra uma issue no repositório.
